package my.tool.code.learning.sort;

public class QuickSort {

	private static void quicksort(int[] array){
		doQuickSearch(array, 0, array.length - 1, 0);
	}
	
	private static void doQuickSearch(int[] array, int start, int end, int time){
		if (start >= end || time == 1000) {
			return;
		}
		int index = pivotPosition(array, start, end);
		time ++;
		doQuickSearch(array, start, index - 1, time);
		doQuickSearch(array, index + 1, end, time);
	}
	
	private static int pivotPosition(int[] array, int start, int end){
		int i = start + 1;
		int j = end;
		int pivot = array[start];
		while (i <= j){
			while (i < j && array[i] < pivot){
				i++;
			}
			while (j > i && array[j] >= pivot){
				j--;
			}
			if (i < j){
				swap(array, i, j);
			}
			if (array[j] >= pivot){
				j--;
			}
			if (array[i] < pivot){
				i++;
			}
		}
		if (j >= 0){
			swap(array, start, j);
		}
		return j;
	}
	
	private static void swap(int[] array, int i, int j){
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}
