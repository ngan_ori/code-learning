package my.tool.code.learning.labyrinth;

public enum Way {
	
	NEXT_DOWN,
	NEXT_UP,
	BACK_DOWN,
	BACK_UP

}
