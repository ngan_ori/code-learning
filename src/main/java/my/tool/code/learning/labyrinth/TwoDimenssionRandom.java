package my.tool.code.learning.labyrinth;

import java.util.Random;

@SuppressWarnings("unused")
public final class TwoDimenssionRandom {
	
	private final static Random random = new Random();
	private int max;
	private int min;

	private TwoDimenssionRandom(int max, int min) {
		this.max = max;
		this.min = min;
	}
	
	public static TwoDimenssionRandom getInstance(int min, int max) {
		return new TwoDimenssionRandom(max, min);
	}
	
	private int random(int min, int max) {
		return random.nextInt((max - min) + 1);
	}
	
	public int[][] initTwoDimenssionArray(int length){
		int[][] result = new int[length][length];
		for (int i = 0 ;  i < length; i++) {
			for (int j = 0; j < length; j ++) {
				result[i][j] = random(0, 1);
			}
		}
		return result;
	}
	

}
