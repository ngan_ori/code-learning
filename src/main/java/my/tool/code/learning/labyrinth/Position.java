package my.tool.code.learning.labyrinth;

import java.util.List;

public class Position {
	
	private int x;
	private int y;
	private List<Position> posibilities;
	
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}

	public void increaseX() {
		x += 1;
	}
	
	public void decreaseX() {
		x-= 1;
	}
	
	public void increaseY() {
		y += 1;
	}
	
	public void decreaseY() {
		y-= 1;
	}
	
	public List<Position> getPosibilities() {
		return posibilities;
	}

	public void setPosibilities(List<Position> posibilities) {
		this.posibilities = posibilities;
	}

	public String toString() {
		return "("+ x + "," + y +")";
	}

	@Override
	public boolean equals(Object obj) {
		Position other = (Position) obj;
		return x == other.x
				&& y == other.y;
	}

}
