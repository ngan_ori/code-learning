package my.tool.code.learning.qpro;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Answer {
	
	private static void printToFile(int[] array, OutputStream out) throws IOException{
		for (int i = 0; i < array.length; i++){
			String data = Integer.toString(array[i]) + System.lineSeparator();
			out.write(data.getBytes());
		}
		out.close();
	}
	
	private static int printMiddle(int[] array, int from, int size){
		int[] smArray = new int [size];
		System.arraycopy(array, from, smArray, 0, size);
		Arrays.parallelSort(smArray);
		return smArray[size/2];
	}

	private static int[] pickArray(List<String> strs, int n) {
		int[] array = new int [n];
		for (int i = strs.size() - 1; i > 0; i--){
			array[i - 1] = Integer.parseInt(strs.get(i));
		}
		return array;
	}
	
	private static List<String> readFileLines(InputStream in){
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		List<String> strs = br.lines().collect(Collectors.toList());
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return strs;
	}
	
	public void answerTo(InputStream input, OutputStream output) throws Exception {
		List<String> strs = readFileLines(input);
		String firstLine = strs.get(0);
		String[] nas = firstLine.split(" ");
		int n = Integer.parseInt(nas[0]);
		int s = Integer.parseInt(nas[1]);
		int[] array = pickArray(strs, n);
		int mSize = n - s + 1;
		int[] middles = new int[mSize];
		for (int i = mSize - 1; i >= 0; i--){
			middles[i] = printMiddle(array, i, s);
		}
		printToFile(middles, output);
    }

}
